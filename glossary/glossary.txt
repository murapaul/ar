# Glossary in tab-separated format -*- coding: utf-8 -*-
paramétrage	إعدادات	Traduction Paramétrage en Arabe | Dictionnaire Français-Arabe | Reverso. https://dictionnaire.reverso.net/francais-arabe/Param%C3%A9trage.
Guide de l'utilisateur.	Guide de l'utilisateur.
F1	F1
OmegaT	OmegaT
OmegaT	أوميجاتي	Source : https://omegat.org/ar/
OmegaT.css	OmegaT.css
GNU General Public License, version 3.0	GNU General Public License, version 3.0
(TAO)	الترجمة بمساعدة الحاسوب	https://dictionnaire.reverso.net/francais-arabe/traduction+assist%C3%A9e+par+ordinateur
OK	OK
Projet	مشروع	Source: Guide arabe omegaT.
Entrée	Enter	Source : Guide arabe omegaT.
segment	المقطع	Source : Guide arabe omegaT.
Nouveau	جديد	Source : Guide arabe omegaT.
mémoire de traduction	ذاكرة الترجمة	Source : Guide arabe omegaT.
Guide de l'utilisateur	دليل البداية الفورية	Souce : Guide arabe omegaT
Aide	مساعدة	Source : Interface du logiciel arabe.
Correspondances	match	Source : Interface du logiciel omegaT arabe.
target	target	Source : Interface du logiciel omegaT arabe.
des fichiers	ملفات المشروع	Source : Interface du logiciel omegaT arabe.
Créer les documents traduits	انشاء المستندات المترجمة	Source : Interface du logiciel omegaT arabe.
Accéder au contenu du projet	Access Project Contents	Source : Interface du logiciel omegaT arabe.
À propos	حول اميجاتي	Source : Interface du logiciel omegaT arabe.
